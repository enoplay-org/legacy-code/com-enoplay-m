const getVideoID = (src) => {
  if (!src) {
    return ''
  }

  src = addProtocol(src)

  if (src.indexOf('youtube-nocookie.com/embed') >= 0) {
    return getPath(src).substring(7)
  } else if (src.indexOf('youtube.com/embed') >= 0) {
    return getPath(src).substring(7)
  } else if (src.indexOf('youtu.be') >= 0 || src.indexOf('y2u.be') >= 0) {
    return getPath(src).substring(1)
  } else if (src.indexOf('youtube') >= 0) {
    return getParameterByName('v', src)
  }

  return ''
}

const getVideoThumbnail = (videoID) => {
  return `https://i.ytimg.com/vi/${videoID}/hqdefault.jpg`
}

const getVideoSrc = (videoID) => {
  return `https://www.youtube-nocookie.com/embed/${videoID}?autoplay=1`
}

function addProtocol (url) {
  if (!url) {
    return ''
  }

  if (url.indexOf('http') >= 0 || url.indexOf('https') >= 0) {
    return url
  }

  return `https://${url}`
}

function getPath (url) {
  let location = document.createElement('a')
  location.href = url
  return location.pathname
}

function getParameterByName (name, url) {
  if (!url) {
    return null
  }
  name = name.replace(/[[\]]/g, '\\$&')
  let regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)')
  let results = regex.exec(url)
  if (!results) {
    return null
  }
  if (!results[2]) {
    return ''
  }
  return decodeURIComponent(results[2].replace(/\+/g, ' '))
}

export {
  getVideoID,
  getVideoThumbnail,
  getVideoSrc
}
