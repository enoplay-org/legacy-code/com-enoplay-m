import Vue from 'vue'
import BrowserSupport from '~components/Play/BrowserSupport'

describe('components/Play/BrowserSupport.vue', () => {
  it('should render "Browser not supported" title', () => {
    const vm = new Vue({
      el: document.createElement('div'),
      render: (h) => h(BrowserSupport)
    })
    expect(vm.$el.querySelector('.text-title').textContent)
      .to.equal('Browser not supported')
  })
})
