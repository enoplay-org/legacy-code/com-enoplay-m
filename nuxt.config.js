var config = require('config')
var env = config.get('env')

const META_SITE_NAME = 'Enoplay'
const META_URL = `${env.HOST_URL_WWW}`
const META_TITLE = 'Enoplay - The Next Generation Game Platform'
const META_DESCRIPTION = 'Play the games you love, upload original games, and share it all with friends, family, and the world on Enoplay.'
const ICON_URL_BASE = '/icon/logo'
const META_ICON = `${env.HOST_URL_WWW}/icon.png`
const META_ICON_48 = `${env.HOST_URL_WWW}${ICON_URL_BASE}/ep-mini-logo_48.png`
const META_ICON_96 = `${env.HOST_URL_WWW}${ICON_URL_BASE}/ep-mini-logo_96.png`
const META_ICON_144 = `${env.HOST_URL_WWW}${ICON_URL_BASE}/ep-mini-logo_144.png`

module.exports = {
  /*
  ** Environment configuration
  */
  env: env,
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLINT on save
    */
    extend (extConfig, { isClient }) {
      if (isClient) {
        extConfig.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    },
    vendor: [
      'axios',
      'localforage',
      'vuelidate',
      '~/utils/dateCalculator',
      '~/utils/device',
      '~/utils/formValidator',
      '~/utils/imageUtil',
      '~/utils/priceUtil',
      '~/utils/stripeUtil',
      '~/utils/youtube'
    ]
  },
  /*
  ** Headers
  ** Common headers are already provided by @nuxtjs/pwa preset
  */
  head: {
    title: 'Enoplay - The Next Generation Game Platform',
    link: [
      { hid: 'alternate-handheld', href: `${env.HOST_URL_MOBILE}`, media: 'handheld' },
      { hid: 'alternate-screen-max-width', href: `${env.HOST_URL_MOBILE}`, media: 'only screen and (max-width: 640px)' },
      { hid: 'shortcut-icon', rel: 'shortcut icon', href: META_ICON },
      { hid: 'icon-48', rel: 'icon', href: META_ICON_48, sizes: '48x48' },
      { hid: 'icon-96', rel: 'icon', href: META_ICON_96, sizes: '96x96' },
      { hid: 'icon-144', rel: 'icon', href: META_ICON_144, sizes: '144x144' }
    ],
    meta: [
      { hid: 'description', name: 'description', content: META_TITLE },
      { hid: 'keywords', name: 'keywords', content: 'game, playing, sharing, online, multiplayer, singleplayer, upload' },
      { hid: 'og:url', property: 'og:url', content: META_URL },
      { hid: 'og:site_name', property: 'og:site_name', content: META_SITE_NAME },
      { hid: 'og:title', property: 'og:title', content: META_TITLE },
      { hid: 'og:description', property: 'og:description', content: META_DESCRIPTION },
      { hid: 'og:image', property: 'og:image', content: META_ICON },
      { hid: 'twitter:card', property: 'twitter:card', content: 'summary' },
      { hid: 'twitter:site', property: 'twitter:site', content: '@enoplay' },
      { hid: 'twitter:url', property: 'twitter:url', content: META_URL },
      { hid: 'twitter:title', property: 'twitter:title', content: META_TITLE },
      { hid: 'twitter:description', property: 'twitter:description', content: META_DESCRIPTION },
      { hid: 'twitter:image', property: 'twitter:image', content: META_ICON }
    ],
    script: [
      { src: 'https://www.google.com/recaptcha/api.js' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#22C19A' },
  /*
  ** Customize app manifest
  */
  manifest: {
    name: 'Enoplay',
    short_name: 'Enoplay',
    display: 'standalone',
    theme_color: '#FC6552',
    background_color: '#F7F5F2',
    description: 'The Next Generation Game Platform',
    icons: [{
      'src': `${ICON_URL_BASE}logo/ep-mini-logo_48.png`,
      'sizes': '48x48',
      'type': 'image/png'
    }, {
      'src': `${ICON_URL_BASE}/ep-mini-logo_72.png`,
      'sizes': '72x72',
      'type': 'image/png'
    }, {
      'src': `${ICON_URL_BASE}/ep-mini-logo_96.png`,
      'sizes': '96x96',
      'type': 'image/png'
    }, {
      'src': `${ICON_URL_BASE}/ep-mini-logo_144.png`,
      'sizes': '144x144',
      'type': 'image/png'
    }, {
      'src': `${ICON_URL_BASE}/ep-mini-logo_168.png`,
      'sizes': '168x168',
      'type': 'image/png'
    }, {
      'src': `${ICON_URL_BASE}/ep-mini-logo_192.png`,
      'sizes': '192x192',
      'type': 'image/png'
    }]
  },
  /*
  ** Modules
  */
  modules: [
    '@nuxtjs/pwa'
  ],
  /*
  ** Plugins
  */
  plugins: [{
    src: '~plugins/vuelidate',
    ssr: false
  }],
  /*
  ** Middleware
  */
  router: {
    middleware: ['userAgent']
  },
  serverMiddleware: ['redirect-ssl']
}
