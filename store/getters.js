import dataTypes from './data-types'

// Games - Active Game
const activeGame = state => {
  let game = state.games.all[state.games.activeGameUID] || dataTypes.defaultGame

  const gid = game.gid
  const thumbnail = game.media.thumbnail.source
  const privacy = game.privacy
  const title = game.title
  const description = game.description
  const price = game.price
  const systemSupport = game.systemSupport
  const metaData = {
    amount: game.stats.playedNow || 0,
    unit: 'plays'
  }

  const app = game.app

  let mediaList = []
  let images = []
  let videos = []

  for (let v in game.media.videos) {
    let video = {
      uid: v,
      type: 'video',
      thumbnail: game.media.videos[v].thumbnail,
      source: game.media.videos[v].source
    }
    mediaList.push(video)
    videos.push(video)
  }

  for (let i in game.media.images) {
    let image = {
      uid: i,
      type: 'image',
      thumbnail: game.media.images[i].source,
      source: game.media.images[i].source
    }
    mediaList.push(image)
    images.push(image)
  }

  let userUID = game.publisher.uid
  let user = state.users.all[userUID] || dataTypes.defaultUser

  const publisher = {
    uid: userUID,
    username: user.username,
    alias: user.alias,
    icon: user.media.icon.source
  }

  return {
    gid,
    thumbnail,
    privacy,
    title,
    description,
    publisher,
    price,
    systemSupport,
    mediaList,
    media: { images, videos },
    app,
    metaData
  }
}

// Games - New Releases
const newReleaseList = state => {
  let gameList = Object.keys(state.games.newReleaseList)
  let browser = state.profile.deviceSettings.browser
  let device = state.profile.deviceSettings.device

  // List games that support this browser and device
  gameList = gameList.filter(gameUID => {
    let game = state.games.all[gameUID] || dataTypes.defaultGame
    return game.systemSupport.browser[browser] && game.systemSupport.browser[browser][device]
  })

  return gameList.map(gameUID => {
    let game = state.games.all[gameUID] || dataTypes.defaultGame

    const gid = game.gid
    const thumbnail = game.media.thumbnail.source
    const title = game.title
    const price = game.price
    const metaData = {
      amount: game.stats.playedNow || 0,
      unit: 'plays'
    }

    let userUID = game.publisher.uid
    let user = state.users.all[userUID] || dataTypes.defaultUser

    const publisher = {
      uid: userUID,
      username: user.username,
      alias: user.alias,
      icon: user.media.icon.source
    }

    return {
      gid,
      thumbnail,
      title,
      publisher,
      price,
      metaData
    }
  })
}

// Games - Tags
const tagsList = state => {
  return Object.keys(state.games.tags).map((uid) => {
    const tid = state.games.tags[uid].tid
    const alias = state.games.tags[uid].alias
    const numOfGames = Object.keys(state.games.tags[uid].games).length
    console.log('tid:', tid)
    return {
      tid,
      alias,
      numOfGames
    }
  })
}

const tagsSortedByAlias = getters => {
  if (!getters.tagsList) {
    return []
  }

  return getters.tagsList.sort((a, b) => {
    var aliasA = a.alias.toUpperCase() // ignore upper and lowercase
    var aliasB = b.alias.toUpperCase() // ignore upper and lowercase
    if (aliasA < aliasB) {
      return -1
    }
    if (aliasA > aliasB) {
      return 1
    }
    // names must be equal
    return 0
  })
}

// Profile - Library
const libraryList = state => {
  return Object.keys(state.profile.library).map((gameUID) => {
    let game = state.games.all[gameUID]
    if (!game) {
      return
    }
    const gid = game.gid
    const title = game.title
    const lastPlayed = state.profile.gameHistory[gameUID].lastPlayed

    return {
      gid,
      title,
      lastPlayed
    }
  })
}

const librarySortedByTitle = getters => {
  if (!getters.libraryList) {
    return []
  }

  return getters.libraryList.sort((a, b) => {
    var titleA = a.title.toUpperCase() // ignore upper and lowercase
    var titleB = b.title.toUpperCase() // ignore upper and lowercase
    if (titleA < titleB) {
      return -1
    }
    if (titleA > titleB) {
      return 1
    }
    // names must be equal
    return 0
  })
}

const librarySortedByLastPlayed = getters => {
  if (!getters.libraryList) {
    return []
  }

  return getters.libraryList.sort((a, b) => {
    return a.lastPlayed - b.lastPlayed
  })
}

export default {
  activeGame,
  newReleaseList,
  tagsList,
  tagsSortedByAlias,
  libraryList,
  librarySortedByTitle,
  librarySortedByLastPlayed
}
