import Vue from 'vue'
import api from '~/api'
import types from '../mutation-types'

import {
  BROWSER_EDGE,
  BROWSER_IE,
  DEVICE_MOBILE
} from '../../utils/device'

import { updateGameThumbnail, updateGameImage } from '../../utils/imageUtil'

// initial state
const state = {
  all: {/* [uid: string]: Game */},
  tags: {/* [uid: string]: Tag */},
  apps: {/* [uid: string]: App */},

  activeTags: [/* tag uids */],
  newReleaseList: {/* [uid: string]: NewReleaseGame */},

  activeGameUID: 'default_game_uid'
}

const getters = {
  activeGameApp (state) {
    let game = state.all[state.activeGameUID]

    if (!game || !game.app || !game.app.uid) {
      return {}
    }

    let app = state.apps[game.app.uid]

    if (!app) {
      return {}
    }

    let prodVersionUID = app.versionsActive.prod
    let devVersionUID = app.versionsActive.dev

    return {
      prodWebURL: app.versions[prodVersionUID].hostDetails.webURL,
      devWebURL: devVersionUID ? app.versions[devVersionUID].hostDetails.webURL : ''
    }
  }
}

const actions = {
  getGameByGID ({ state, commit }, gid) {
    let uid = null

    for (let g in state.all) {
      if (state.all[g].gid.toLowerCase() === gid.toLowerCase()) {
        uid = state.all[g].uid
        break
      }
    }

    if (uid && state.all[uid]) {
      return Promise.resolve(state.all[uid])
    }

    return api.getGameByGID(gid)
    .then(game => {
      commit(types.SET_GAME, game)
      return game
    })
    .catch(err => {
      return Promise.reject(`${err}`)
    })
  },
  getGameByGIDWithForce ({ state, commit }, gid) {
    return api.getGameByGID(gid)
    .then(game => {
      commit(types.SET_GAME, game)
      return game
    })
    .catch(err => {
      return Promise.reject(`${err}`)
    })
  },
  getGamesByUIDs ({ state, commit }, uids) {
    let uids2 = uids.filter(uid => {
      if (!state.all[uid]) {
        return true
      }
    })

    if (uids2.length < 1) {
      return Promise.resolve(uids.map(key => state.all[key]))
    }

    return api.getGamesByUIDs(uids2)
    .then(games => {
      commit(types.SET_GAMES, games)
      return games.map(g => state.all[g.uid])
    })
    .catch(err => {
      return Promise.reject(`${err}`)
    })
  },
  getGamesByUIDsWithForce ({ state, commit }, uids) {
    if (uids.length < 1) {
      return Promise.resolve(uids.map(key => state.all[key]))
    }

    return api.getGamesByUIDs(uids)
    .then(games => {
      commit(types.SET_GAMES, games)
      return games.map(g => state.all[g.uid])
    })
    .catch(err => {
      return Promise.reject(`${err}`)
    })
  },
  deleteGame ({ state }, gid) {
    return api.deleteGame(gid)
  },
  getGameApp ({ state, commit }, aid) {
    return api.getGameApp(aid)
    .then(appResponse => {
      commit(types.SET_APP, appResponse)
      return appResponse
    })
    .catch(err => {
      return Promise.reject(`${err}`)
    })
  },
  getGameAppStatus ({ state }, aid) {
    return api.getGameAppStatus(aid)
    .catch(err => {
      return Promise.reject(`Error retrieving app status for aid ${aid}: ${err}`)
    })
  },
  updateGameApp ({ state, commit }, { gid, app }) {
    let appGame = Object.keys(state.all)
      .map(key => state.all[key])
      .filter(game => game.gid === gid)[0]
    let aid = appGame.app.aid
    return api.updateGameAppFile(aid, app)
    .then(appResponse => {
      commit(types.SET_APP, appResponse)
      return appResponse
    })
    .catch(err => {
      return Promise.reject(`${err}`)
    })
  },
  updateGameAppVersion ({ state, commit }, { gid, version }) {
    let appGame = Object.keys(state.all)
      .map(key => state.all[key])
      .filter(game => game.gid === gid)[0]
    let aid = appGame.app.aid
    return api.updateGameAppVersion(aid, version)
    .then(appResponse => {
      commit(types.SET_APP, appResponse)
      return appResponse
    })
    .catch(err => {
      return Promise.reject(`${err}`)
    })
  },
  updateGameTitle ({ state }, { gid, title }) {
    return api.updateGameTitle(gid, title)
  },
  udpateGameDescription ({ state }, { gid, description }) {
    return api.udpateGameDescription(gid, description)
  },
  updateGamePrice ({ state }, { gid, price }) {
    return api.updateGamePrice(gid, price)
  },
  updateGamePrivacy ({ state }, { gid, privacy }) {
    return api.updateGamePrivacy(gid, privacy)
  },
  updateGameBrowserSupport ({ state }, { gid, support }) {
    return api.updateGameBrowserSupport(gid, support)
  },
  updateGameThumbnail ({ state }, { gid, thumbnail }) {
    return api.updateGameThumbnail(gid, thumbnail)
  },
  udpateGameImages ({ state }, { gid, images }) {
    return api.updateGameImages(gid, images)
  },
  updateGameVideos ({ state }, { gid, videos }) {
    return api.updateGameVideos(gid, videos)
  },
  getTagsByUIDs ({ commit }, uids) {
    let uids2 = uids.filter(uid => {
      if (!state.all[uid]) {
        return true
      }
    })

    if (uids2.length < 1) {
      return Promise.resolve(uids.map(key => state.all[key]))
    }

    return api.getTagsByUIDs(uids2)
    .then(tags => {
      commit(types.SET_TAGS, tags)
      return tags
    })
    .catch(err => {
      return Promise.reject(`${err}`)
    })
  }
}

const mutations = {
  [types.SET_ACTIVE_GAME] (state, uid) {
    state.activeGameUID = uid
  },
  [types.SET_NEW_RELEASE_LIST] (state, games) {
    games.forEach(game => {
      if (game) {
        Vue.set(state.newReleaseList, game.uid, true)
      }
    })
  },
  [types.SET_GAMES] (state, games) {
    games.forEach(game => {
      if (!game) {
        return
      }

      if (game.media && game.media.thumbnail) {
        game.media.thumbnail.source = updateGameThumbnail(game.media.thumbnail.source)
      }

      if (game.media && game.media.images) {
        Object.keys(game.media.images).filter(image => {
          return updateGameImage(game.media.images[image].source)
        })
      }

      // Temporarily disable support for ie and edge on mobile
      if (game.systemSupport.browser) {
        game.systemSupport.browser[BROWSER_EDGE][DEVICE_MOBILE] = false
        game.systemSupport.browser[BROWSER_IE][DEVICE_MOBILE] = false
      }

      Vue.set(state.all, game.uid, game)
    })
  },
  [types.SET_GAME] (state, game) {
    if (!game) {
      return
    }

    // Temporarily disable support for ie and edge on mobile
    if (game.systemSupport.browser) {
      game.systemSupport.browser[BROWSER_EDGE][DEVICE_MOBILE] = false
      game.systemSupport.browser[BROWSER_IE][DEVICE_MOBILE] = false
    }

    Vue.set(state.all, game.uid, game)
  },
  [types.SET_APP] (state, app) {
    if (!app) {
      return
    }

    Vue.set(state.apps, app.uid, app)
  },
  [types.SET_TAGS] (state, tags) {
    tags.forEach(tag => {
      if (tag) {
        Vue.set(state.tags, tag.uid, tag)
      }
    })
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
