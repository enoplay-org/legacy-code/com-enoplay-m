import Vue from 'vue'
import Vuex from 'vuex'
import actions from './actions'
import getters from './getters'
import app from './modules/app'
import profile from './modules/profile'
import users from './modules/users'
import games from './modules/games'

Vue.use(Vuex)

const store = () => new Vuex.Store({
  actions,
  getters,
  modules: {
    app,
    profile,
    users,
    games
  }
})

export default store
